package com.bettercoding.dl4j.controller;

import com.bettercoding.dl4j.component.UIServerComponent;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.util.Duration;
import javafx.util.converter.NumberStringConverter;
import org.deeplearning4j.util.ModelSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class LearningGuiController {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final IntegerProperty counterProperty = new SimpleIntegerProperty();

    @FXML
    private Button btnLoad;
    @FXML
    private Button btnSave;
    @FXML
    private ToggleButton btnTrain;
    @FXML
    private Button btnTest;

    @FXML
    private Label counterText;

    private Timeline guiRefreshTimeline;

    @Value("${nn-file}")
    private String nnFile;

    @Value("${gui-refresh-interval-ms}")
    private double guiRefreshIntervalMs;

    @Autowired
    private UIServerComponent uiServerComponent;

    @Autowired
    private CustomLearningGuiController customLearningGuiController;

    @FXML
    private void initialize() {
        guiRefreshTimeline = new Timeline(new KeyFrame(Duration.millis(guiRefreshIntervalMs), event -> {
            customLearningGuiController.onRefreshGUI();
        }));
        guiRefreshTimeline.setCycleCount(Timeline.INDEFINITE);

        counterText.textProperty().bindBidirectional(counterProperty, new NumberStringConverter("Train loop: ######"));

        customLearningGuiController.onInitialize();

        uiServerComponent.reinitialize(customLearningGuiController.onGetNeuralNetwork());
        guiRefreshTimeline.play();
    }


    public void loadAction(ActionEvent actionEvent) {
        onLoadAction();
    }

    protected void onLoadAction() {
        try {
            customLearningGuiController.onSetNeuralNetwork(ModelSerializer.restoreMultiLayerNetwork(nnFile));
            uiServerComponent.reinitialize(customLearningGuiController.onGetNeuralNetwork());
            showAlert(Alert.AlertType.INFORMATION, "Success", "Neural network successfully loaded from " + nnFile);
        } catch (IOException e) {
            logger.error("Loading neural network error", e);
            showAlert(Alert.AlertType.ERROR, "Loading neural network error", e.getMessage());
        }
    }

    public void saveAction(ActionEvent actionEvent) {
        onSaveAction();
    }

    protected void onSaveAction() {
        try {
            ModelSerializer.writeModel(customLearningGuiController.onGetNeuralNetwork(), nnFile, true);
            showAlert(Alert.AlertType.INFORMATION, "Success", "Neural network successfully saved to " + nnFile);
        } catch (IOException e) {
            logger.error("Saving neural network error", e);
            showAlert(Alert.AlertType.ERROR, "Saving neural network error", e.getMessage());
        }
    }

    public void trainAction(ActionEvent actionEvent) {
        customLearningGuiController.onTrainAction();
        boolean trainingMode = btnTrain.isSelected();
        btnLoad.setDisable(trainingMode);
        btnSave.setDisable(trainingMode);
        btnTest.setDisable(trainingMode);
        if (btnTrain.isSelected()) {
            Platform.runLater(this::trainLoop);
        }
    }


    public void testAction(ActionEvent actionEvent) {
        try {
            customLearningGuiController.onTestAction();
        } catch (RuntimeException e) {
            logger.error("Test execution error", e);
            showAlert(Alert.AlertType.ERROR, "Test execution error", e.getMessage());
        }
    }


    private void trainLoop() {
        if (btnTrain.isSelected()) {
            counterProperty.setValue(counterProperty.get() + 1);
            long loopNo = counterProperty.get();
            logger.debug("Train Loop: {}", loopNo);
            customLearningGuiController.onTrainLoop(loopNo);
            Platform.runLater(this::trainLoop);
        }
    }


    public void onCloseRequest() {
        uiServerComponent.stop();
        btnTrain.setSelected(false);
    }

    protected void showAlert(Alert.AlertType alertType, String title, String content) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(content);

        alert.showAndWait();
    }

}
