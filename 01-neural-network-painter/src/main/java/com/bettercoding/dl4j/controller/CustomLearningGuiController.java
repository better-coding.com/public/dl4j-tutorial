package com.bettercoding.dl4j.controller;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;

public interface CustomLearningGuiController {
    void onRefreshGUI();

    void onInitialize();

    void onTrainLoop(long loopNo);

    void onTestAction();

    void onTrainAction();

    MultiLayerNetwork onGetNeuralNetwork();

    void onSetNeuralNetwork(MultiLayerNetwork restoreMultiLayerNetwork);
}
